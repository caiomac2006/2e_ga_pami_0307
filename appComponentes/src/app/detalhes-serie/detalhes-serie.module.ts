import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalhesSeriePageRoutingModule } from './detalhes-serie-routing.module';

import { DetalhesSeriePage } from './detalhes-serie.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalhesSeriePageRoutingModule
  ],
  declarations: [DetalhesSeriePage]
})
export class DetalhesSeriePageModule {}
